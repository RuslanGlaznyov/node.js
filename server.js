const  express = require('express');
const bodyparser = require('body-parser');
let tasks = [];
tasks =  require('./tasks');
// import tasks from "./tasks" ;
tasks = tasks.tasks;

app = express();

app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended: true}));

app.get('/', function(req, res){
	res.end('Trello');
});
// получение всех тасков
app.get('/api/task', function(req, res){
	if (tasks.length !== 0){
		res.send(tasks);
	}else{
		res.sendStatus(204).send({'error': 'task not found'});
	}
});

// получение тасков по id
app.get('/api/task/:id', function(req, res){
	var task = tasks.find(function(task){
		return task.id === Number(req.params.id);
	});
	if (task === undefined){
		res.sendStatus(204).send({'error':'task not found'});
	}else{
		res.send(task)
	}
	
});

app.post('/api/task', function(req, res){
	var task = {
		id: Date.now(),
		title: req.body.title,
		desc: req.body.desc,
		finished: req.body.finished,
	}
	
	addData(task, res);
});

app.put('/api/task/:id', function(req, res){
	var task = tasks.find(function(task){
		return task.id === Number(req.params.id);
	});
	if(task == undefined){
		task.desc = req.body.desc === undefined ? task.desc : req.body.desc  ;
		task.title = req.body.title === undefined ? task.title : req.body.title ;
		task.finished = req.body.finished === undefined ? task.finished : req.body.finished;
		res.status(200).send(task);
	}else{
		res.sendStatus(400);
	}
});
app.delete('/api/task/:id', function(req, res){
	if(tasks = undefined){
	tasks = tasks.filter(function(task){
		return task.id !== Number(req.params.id);
	})
	res.sendStatus(200);
	}else{
		res.sendStatus(400);
	}
});

app.listen(3000, function(){
	console.log('server run')
});
function addData(task, res){
	titleError = {};
	if(task.title.length === 0){
		titleError.error = 'title is empty'; 
		res.status(405).send(titleError);
		return	
	}
	if(typeof(task.title) !== 'string'){
		titleError.error = 'title is not string';
		res.status(405).send(titleError);
		return	
	}
	validErrors = getErrors(task);
	if (Object.keys(validErrors).length === 0 && validErrors.constructor === Object){
		tasks.push(task);
		res.status(201).send(task);
	}else{
		res.status(405).send(validErrors);
	}
}
function getErrors(task){
	errors = {};
	
	if (typeof(task.finished) !== 'boolean'){
		errors.finished = "is not boolean";
   	}
	for(var key in task){ 
		if (task[key] === undefined) {
			errors[key] = "is empty field";
		}
	}
	return errors;
}





